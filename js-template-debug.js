/*****
 debug 版本是为了开发过程中方便异常处理,定位到清晰的出错代码

 作者:浮山狼
 邮箱:fushanlang@gmail.com
 若使用中遇到问题，欢迎交流、切磋！
 */

function js_template(template, values) {


    //唯一分隔标志字符串
    var split = '_{' + Math.random() + '}_';

    //消除换行符
    var estr = template.replace(/\n|\r|\t/g, "");

    var js = [];
    /****
     * 匹配标签<js> ...</js>--并且替换为特定的分隔串，
     * 并将匹配的js代码放入js数组中备用
     * */
    estr = estr.replace(/<js>(.*?)<\/js>/g, function ($0, $1) {
        js.push($1);
        return split;
    });

    var txt = estr.split(split);
    estr = " var output='';";
    /****
     * 0101010---0为txt[]元素,1为js[]元素
     * 重新串起来连接为可执行eval的estr
     * **/
    for (var i = 0; i < js.length; ++i) {
        estr += 'output+="'+txt[i]+'";\n\r';
        estr += js[i]+'\n\r';

    }
    estr += 'output+="'+txt[js.length]+'";';

    estr += 'return output;';
    //return;
    return  Function('vars',estr);
}