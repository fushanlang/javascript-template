/*****

新版js_template出炉,去掉了内置函数,
强化了指定的模板变量vars,和输出变量output. 
加上了模板预编译,js_template当前返回是编译好的模板函数,
而非直接返回模板与数据的绑定结果.

----------------------
 模板字符串，语法标签采用<js></js>
 完全遵循javascript原生语法，内置输出函数为echo(string);函数


 常用调用例子，可以用 如果下方法.
 <textarea id="template_code" style="display:none'>
 <js> for(var i=0;i<names.length;++i){</js>
 	<label><h1><js> echo(names[i]);</js></h1></label>
 <js> }</js>
 </textarea>
 js_template(document.getElementById('template_code').value,{names:['tom','jerry','david']});

 异常处理:
 eval解析异常会往window.AJ变量里面记录debug信息方便调试。

 作者:浮山狼
 邮箱:fushanlang@gmail.com
 若使用中遇到问题，欢迎交流、切磋！
 */

function js_template(template, vars) {
    
    //唯一分隔标志字符串
    var split = '_{' + Math.random() + '}_';

    //消除换行符
    var estr = template.replace(/\n|\r|\t/g, "");

    var js = [];
    /****
     * 匹配标签<js> ...</js>--并且替换为特定的分隔串，
     * 并将匹配的js代码放入js数组中备用
     * */
    estr = estr.replace(/<js>(.*?)<\/js>/g, function ($0, $1) {
        js.push($1);
        return split;
    });

    /*根据特定的分隔串分隔得到普通text文本串的数组*/
    var text = estr.split(split);
    estr = " var output='';";
    /****
     * 0101010---0为text[]元素,1为js[]元素
     * 重新串起来连接为可执行eval的estr
     * **/
    for (var i = 0; i < js.length; ++i) {
        estr += 'output+=text[' + i + '];';
        estr += js[i];

    }
    estr += 'output+=text[' + js.length + '];';

    estr += 'return output;';

    var fun =new Function('vars','text',estr);
    return function(data){
        return fun.apply(null,[data,text]);
    }
}