#javascript template


模板字符串，语法标签采用&lt;js&gt;&lt;/js&gt;
完全遵循javascript原生语法，内置输出函数为echo(string);函数


常用调用例子，可以用 如果下方法.
&lt;textarea id="template_code" style="display:none'&gt;
&lt;js&gt; for(var i=0;i&lt;names.length;++i){&lt;/js&gt;
&lt;label&gt;&lt;h1&gt;&lt;js&gt; echo(names[i]);&lt;/js&gt;&lt;/h1&gt;&lt;/label&gt;
&lt;js&gt; }&lt;/js&gt;
&lt;/textarea&gt;
js_template(document.getElementById('template_code').value,{names:['tom','jerry','david']});

异常处理:
eval解析异常会往window.AJ变量里面记录debug信息方便调试。

作者:浮山狼
邮箱:fushanlang@gmail.com
若使用中遇到问题，欢迎交流、切磋！